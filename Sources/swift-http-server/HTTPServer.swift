//
//  HTTPServer.swift
//  HTTPServer
//
//  Created by Nikolay Petrov on 7/7/16.
//  Copyright © 2016 Swift. All rights reserved.
//

import Foundation
import Dispatch

import swift_gcd_server
import swift_http

public typealias HTTPHandler = ((UUID, HTTPRequest) -> Void)

public struct HTTPConfiguration {
    public let gcdConfiguration: GCDConfiguration
    public var port: UInt16 {
        return gcdConfiguration.port
    }

    public init(port: UInt16) {
        self.gcdConfiguration = GCDConfiguration(port: port)
    }
}

struct HTTPConnection {
    let connection: GCDConnection
    let queue: DispatchQueue
    var uuid: UUID { return connection.uuid }

    init(connection: GCDConnection) {
        self.connection = connection
        self.queue = DispatchQueue(label: "http.\(connection.uuid)")
    }
}

public class HTTPServer {
    public var handler: HTTPHandler = { _ in }

    fileprivate let tcpServer: GCDServer
    fileprivate var connections: [UUID: HTTPConnection] = [:]

    public init(configuration: HTTPConfiguration) {
        tcpServer = GCDServer(configuration: configuration.gcdConfiguration)
        tcpServer.connectedCallback = connected
        tcpServer.disconnectedCallback = disconnected

        handler = { [weak self] (connectionID, request) in
            var response = HTTPResponse.ok()
            response.Connection = "close"
            response.ContentType = "text/plain"
            response.message = "No request handler attached"
            self?.send(connectionID: connectionID, response: response)
        }
    }
}

extension HTTPServer {
    public func start() throws {
        try tcpServer.start()
    }

    public func stop() {
        tcpServer.stop()
    }
}

extension HTTPServer {
    public func send(connectionID: UUID, response: HTTPResponse) {
        guard let httpConnection = connections[connectionID] else { return print("error") }

        httpConnection.queue.async {
            httpConnection.connection.send(data: response.serilize())

            if response.Connection == "close" {
                httpConnection.connection.disconnect()
            }
        }
    }
}

private extension HTTPServer {
    func connected(connection: GCDConnection) {
        connections[connection.uuid] = HTTPConnection(connection: connection)
        connection.receivedCallback = { (data) in
            self.received(connection: connection, data: data)
        }
    }

    func received(connection: GCDConnection, data: Data) {
        guard let httpConnection = connections[connection.uuid] else { return print("error") }
        httpConnection.queue.async {
            guard let str = String(data: data, encoding: String.Encoding.utf8) else { return }

            switch HTTPRequestParser().parse(data: str) {
            case HTTPRequestParserResult.More:
                return
            case HTTPRequestParserResult.Error(let message):
                print("[http] received parse error \(message)")

                let response = HTTPResponse.badRequest()
                connection.send(data: response.serilize())

                return
            case HTTPRequestParserResult.Done(let request):
                return self.handler(connection.uuid, request)
            }
        }
    }

    func disconnected(connection: GCDConnection) {
        connections.removeValue(forKey: connection.uuid)
        connection.receivedCallback = nil
        connection.disconnectedCallback = nil
    }
}
