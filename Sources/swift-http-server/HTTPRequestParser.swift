//
//  HTTPRequestParser.swift
//  HTTPServer
//
//  Created by Nikolay Petrov on 6/8/16.
//  Copyright © 2016 Swift. All rights reserved.
//

import Foundation
import swift_http

enum HTTPRequestParserResult {
    case More
    case Error(String)
    case Done(HTTPRequest)
}

protocol Singleton {
    static var instance: Self { get }
}

struct HTTPRequestBuilder {
    var method: HTTPMethod?
    var uri: URL?
    var version: HTTPVersion?

    var headers: [String: String] = [:]

    var message: String?

    var contentLength: Int? {
        return headers["Content-Length"].flatMap { Int($0) }
    }

    func build() -> HTTPRequest {
        return HTTPRequest(method: method!, uri: uri!, version: version!, headers: headers, message: message)
    }
}

class HTTPRequestParser {
    private(set) var result = HTTPRequestParserResult.More

    private var context = HTTPParserContext()
    private var state: HTTPRequestParserState = MethodState.instance

    func parse(data: String) -> HTTPRequestParserResult {
        guard case .More = result else { return result }

        var localContext = context
        var localState = state

        localContext.more(newData: data)

        while true {
            switch localState.parse(context: localContext) {
            case .Continue(let newContext, let newState):
                localContext = newContext
                localState = newState
                continue
            case .More(let newContext, let newState):
                result = .More
                context = newContext
                state = newState
                return result
            case .Halt(let error):
                result = .Error(error)
                context = localContext
                state = localState
                return result
            case .Done(let finalContext):
                result = .Done(finalContext.request.build())
                context = finalContext
                state = localState
                return result
            }
        }
    }
}

struct HTTPParserContext {
    var request: HTTPRequestBuilder
    var data: String
    var currentRange: Range<String.Index>

    init() {
        request = HTTPRequestBuilder()
        data = ""
        currentRange = data.startIndex..<data.endIndex
    }

    mutating func more(newData: String) {
        data.append(newData)
        currentRange = currentRange.lowerBound..<data.endIndex
    }

    mutating func splitData(str: String) -> String? {
        guard let targetRange = data.range(of: str, range: currentRange) else { return nil }

        let result = data.substring(with: currentRange.lowerBound..<targetRange.lowerBound)
        currentRange = targetRange.upperBound..<data.endIndex
        return result
    }

    mutating func crlfPrefix() -> Bool {
        guard let targetRange = data.range(of: "\r\n", range: currentRange) else { return false }

        currentRange = targetRange.upperBound..<data.endIndex
        return true
    }
}

enum HTTPRequestParserStateResult {
    case Continue(HTTPParserContext, HTTPRequestParserState)
    case More(HTTPParserContext, HTTPRequestParserState)
    case Halt(String)
    case Done(HTTPParserContext)
}

protocol HTTPRequestParserState {
    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult
}

extension HTTPRequestParserState {
    func more(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        return .More(context, self)
    }

    func error(message: String) -> HTTPRequestParserStateResult {
        return .Halt(message)
    }

    func done(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        return .Done(context)
    }
}

struct MethodState: HTTPRequestParserState {
    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        var context = context

        guard let methodStr = context.splitData(str: " ") else { return more(context: context) }
        guard let method = HTTPMethod(rawValue: methodStr) else { return error(message: "Unknown method \(methodStr)") }
        context.request.method = method

        return .Continue(context, URIState.instance)
    }
}
extension MethodState: Singleton {
    static let instance = MethodState()
}

struct URIState: HTTPRequestParserState {
    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        var context = context

        guard let uriStr = context.splitData(str: " ") else { return more(context: context) }
        guard let uri = URL(string: uriStr) else { return error(message: "Cannot make uri \(uriStr)") }
        context.request.uri = uri

        return .Continue(context, VersionState.instance)
    }
}
extension URIState: Singleton {
    static let instance = URIState()
}

struct VersionState: HTTPRequestParserState {
    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        var context = context

        guard let versionStr = context.splitData(str: "\r\n") else { return more(context: context) }
        guard let version = HTTPVersion(rawValue: versionStr) else { return error(message: "Unknown version: \(versionStr)") }
        context.request.version = version


        return .Continue(context, HeaderState.instance)
    }
}
extension VersionState: Singleton {
    static let instance = VersionState()
}

struct HeaderState: HTTPRequestParserState {
    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        var context = context

        if context.crlfPrefix() {
            guard let expectedLength = context.request.contentLength else { return done(context: context) }

            return .Continue(context, MessageState(expectedLength: expectedLength))
        }

        guard let header = context.splitData(str: ":") else { return more(context: context) }

        return .Continue(context, ValueState(header: header))
    }
}
extension HeaderState: Singleton {
    static let instance = HeaderState()
}

struct ValueState: HTTPRequestParserState {
    let header: String

    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        var context = context

        guard let value = context.splitData(str: "\r\n") else { return more(context: context) }
        context.request.headers[header] = value

        return .Continue(context, HeaderState.instance)
    }
}

struct MessageState: HTTPRequestParserState {
    let expectedLength: Int

    func parse(context: HTTPParserContext) -> HTTPRequestParserStateResult {
        guard let expectedLength = context.request.contentLength else { return error(message: "No content-length set") }

        var context = context

        let distance = context.data.distance(from: context.currentRange.lowerBound, to: context.currentRange.upperBound)
        if distance < expectedLength {
            return more(context: context)
        }
        if distance > expectedLength {
            return error(message: "message too big")
        }
        
        context.request.message = context.data.substring(with: context.currentRange)
        
        return .Done(context)
    }
}
