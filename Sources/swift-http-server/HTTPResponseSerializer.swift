//
//  HTTPResponseSerializer.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation
import swift_http

let CONTENT_LENGTH = "Content-Length: ".data(using: String.Encoding.utf8)!
let SPACE = " ".data(using: String.Encoding.utf8)!
let CRLF = "\r\n".data(using: String.Encoding.utf8)!
let COL_SPACE = ": ".data(using: String.Encoding.utf8)!

public extension HTTPResponse {
    func serializeHeaders() -> String {
        var str = "\(version.rawValue) \(status.code) \(status.reason)\r\n"

        for (header, value) in headers {
            str.append(header)
            str.append(":")
            str.append(value)
            str.append("\r\n")
        }

        return str
    }

    func serilize() -> Data {
        var data = serializeHeaders().data(using: String.Encoding.utf8)!

        let messageData = message?.data(using: String.Encoding.utf8)
        if let messageData = messageData {
            // check if its different
            if ContentLength == nil {
                data.append(CONTENT_LENGTH)
                data.append("\(messageData.count)".data(using: String.Encoding.utf8)!)
                data.append(CRLF)
            }
        }

        data.append(CRLF)

        if let message = messageData {
            data.append(message)
        }
        
        return data
    }
}

