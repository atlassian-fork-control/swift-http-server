//
//  HTTPStatus.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation

public struct HTTPStatus {
    public let code: UInt
    public let reason: String

    init(code: UInt, reason: String) {
        self.code = code
        self.reason = reason
    }

    public static let code_100 = HTTPStatus(code: 100, reason: "Continue")
    public static let code_101 = HTTPStatus(code: 101, reason: "Switching Protocols")

    public static let code_200 = HTTPStatus(code: 200, reason: "OK")
    public static let code_201 = HTTPStatus(code: 201, reason: "Created")
    public static let code_202 = HTTPStatus(code: 202, reason: "Accepted")
    public static let code_203 = HTTPStatus(code: 203, reason: "Non-Authoritative Information")
    public static let code_204 = HTTPStatus(code: 204, reason: "No Content")
    public static let code_205 = HTTPStatus(code: 205, reason: "Reset Content")
    public static let code_206 = HTTPStatus(code: 206, reason: "Partial Content")

    public static let code_300 = HTTPStatus(code: 300, reason: "Multiple Choices")
    public static let code_301 = HTTPStatus(code: 301, reason: "Moved Permanently")
    public static let code_302 = HTTPStatus(code: 302, reason: "Found")
    public static let code_303 = HTTPStatus(code: 303, reason: "See Other")
    public static let code_304 = HTTPStatus(code: 304, reason: "Not Modified")
    public static let code_305 = HTTPStatus(code: 305, reason: "Use Proxy")
    public static let code_307 = HTTPStatus(code: 307, reason: "Temporary Redirect")

    public static let code_400 = HTTPStatus(code: 400, reason: "Bad Request")
    public static let code_401 = HTTPStatus(code: 401, reason: "Unauthorized")
    public static let code_402 = HTTPStatus(code: 402, reason: "Payment Required")
    public static let code_403 = HTTPStatus(code: 403, reason: "Forbidden")
    public static let code_404 = HTTPStatus(code: 404, reason: "Not Found")
    public static let code_405 = HTTPStatus(code: 405, reason: "Method Not Allowed")
    public static let code_406 = HTTPStatus(code: 406, reason: "Not Acceptable")
    public static let code_407 = HTTPStatus(code: 407, reason: "Proxy Authentication Required")
    public static let code_408 = HTTPStatus(code: 408, reason: "Request Time-out")
    public static let code_409 = HTTPStatus(code: 409, reason: "Conflict")
    public static let code_410 = HTTPStatus(code: 410, reason: "Gone")
    public static let code_411 = HTTPStatus(code: 411, reason: "Length Required")
    public static let code_412 = HTTPStatus(code: 412, reason: "Precondition Failed")
    public static let code_413 = HTTPStatus(code: 413, reason: "Request Entity Too Large")
    public static let code_414 = HTTPStatus(code: 414, reason: "Request-URI Too Large")
    public static let code_415 = HTTPStatus(code: 415, reason: "Unsupported Media Type")
    public static let code_416 = HTTPStatus(code: 416, reason: "Requested range not satisfiable")
    public static let code_417 = HTTPStatus(code: 417, reason: "Expectation Failed")

    public static let code_500 = HTTPStatus(code: 500, reason: "Internal Server Error")
    public static let code_501 = HTTPStatus(code: 501, reason: "Not Implemented")
    public static let code_502 = HTTPStatus(code: 502, reason: "Bad Gateway")
    public static let code_503 = HTTPStatus(code: 503, reason: "Service Unavailable")
    public static let code_504 = HTTPStatus(code: 504, reason: "Gateway Time-out")
    public static let code_505 = HTTPStatus(code: 505, reason: "HTTP Version not supported")
}
